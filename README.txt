Los casos de test se pueden encontrar en el siguiente repositorio: https://gitlab.com/vicentevieytes/algo3-tp2


Instrucciones de compilación:

Buildear cada ejercicio por separado usando el siguiente comando:

$g++ -O3 [EjX.cpp] -o [nombre]

Donde [EjX.cpp] es el archivo del ejercicio X y [nombre], el nombre con el que
se creará al archivo ejecutable. 

Para correr los distintos ejercicios, ingresar por consola:

$./[nombre] [parametro]

siendo [nombre] el nombre del archivo a ejecutar y [parametro] la instancia
a pasar por parametro.
