#include <iostream>
#include <fstream>
#include <vector>
#include <stdlib.h>
#include <string>
#include <algorithm>
#include <list>
#include <queue>
#include <chrono>
using namespace std;

/*
1- Crear las listas de adjacencia y el struct
2- Parsear la info
3- Codear el BFS
4- Modificarlo
*/
//template < class T, class Alloc = allocator<T> > class list;

/*
Vamos a tener que ir guardándolo a medida que hacemos los BFSs
Mientras hacemos el BFS del i-ésimo vértice vamos completando el vector que nos piden imprimir como i-ésima línea
Debería ser fácil, al mismo tiempo que le asignamos parent al k-ésimo nodo, guardamos ese mismo parent en vector[k]
Y en el caso de que no sea geodésico, simplemente hay que parar el BFS e ir haciendo .parent desde los vértices en los que estamos para reconstruir los dos caminos
*/

struct verticeBFS{
    int id; //TODO no entiendo para qué ponés esto si después te queda en 0 en todos los vértices. Creo que es lo que causa que no devuelva la rta correcta
    string color;
    int padre;
    int dist;
    vector<int> camino;
    vector<int> lineaSalida;
};

struct grafo {
    vector<list<int>> adj;
    vector<verticeBFS> vertices;
};

vector<int> bfs(grafo& G, int raiz){
    for (int i = 0; i < G.vertices.size(); i++){
      G.vertices[i].color = "blanco";
      G.vertices[i].dist  = -1;
      G.vertices[i].padre = -1;
      G.vertices[i].camino = {};
    };
    G.vertices[raiz].color = "gris";
    G.vertices[raiz].dist  = 0;
    G.vertices[raiz].padre = -1;
    G.vertices[raiz].camino = {};
    queue<verticeBFS> Q;
    Q.push(G.vertices[raiz]);
    vector<int> lineaSalida(G.vertices.size(), 0); //TODO inicializar el vector con el tamaño y valores
    lineaSalida[raiz] = raiz;
    while (!(Q.empty())){
        verticeBFS u = Q.front();
        Q.pop();
        for (std::list<int>::iterator it=G.adj[u.id].begin(); it != G.adj[u.id].end(); ++it){
            if (G.vertices[*it].color == "blanco" || (G.vertices[*it].color == "gris" && G.vertices[*it].dist == u.dist + 1)){
                if (G.vertices[*it].color == "blanco")
                {
                    G.vertices[*it].color = "gris";
                    G.vertices[*it].dist  = u.dist + 1;
                    G.vertices[*it].padre = u.id;
                    lineaSalida[G.vertices[*it].id] = u.id; // Esto setea en la posicion J de la linea de salida el padre de J en el camino de i a J
                    Q.push(G.vertices[*it]);
                }else{ 
                    // {-1, padre1, padre2, nodoActual}
                    u.camino.push_back(u.id);
                    vector<int> res;
                    res.push_back(-1);
                    res.push_back(u.id);
                    res.push_back(G.vertices[*it].padre);
                    res.push_back(G.vertices[*it].id);
                    return res;
                }
            }
        u.color = "negro";
        }
    }
    return lineaSalida;
}

int main(int argc, char const *argv[])
{
    auto t_start = std::chrono::high_resolution_clock::now();
    if (argc != 2) {
      cout << "Uso: ej1 [filename] " << "\n"<<"Provea un único archivo";
      exit(0);
    }
    ifstream instancia(argv[1]);
    if (! instancia.is_open()){
      cout << "Error abriendo archivo " << argv[1];
      exit(1);
    }

    // Parseo la informacion e inicializo el grafo
    int vertices;
    int aristas;
    instancia >> vertices >> aristas;
    
    grafo G;
    G.vertices = vector<verticeBFS>(vertices);
    for(int i = 0; i<vertices; i++){ // TODO le agregué esto para ponerle el id a los vértices
        G.vertices[i].id = i;
    }
    G.adj = vector<list<int>>(vertices); // tengo que inicializar el primero como si mismo?
    int u,v;
    while(instancia >> u >> v){
        G.adj[u].push_back(v);
        G.adj[v].push_back(u);
    }
   /* parseo la info*/
    vector<vector<int>> salida;
    salida.push_back({1});
    for (int i = 0; i < G.vertices.size(); i++){
        vector<int> res = bfs(G, i);
        if (res[0] == -1){
            // aca tengo que devolver los otros dos caminos
            salida.clear();
            salida.push_back({0});
            // DEVUELVO LOS DOS CAMINOS MINIMOS
            vector<int> camino1;
            vector<int> camino2;
            camino1.push_back(res[3]);
            camino2.push_back(res[3]);
            verticeBFS vertice1 = G.vertices[res[1]];
            verticeBFS vertice2 = G.vertices[res[2]];
            // Armo el camino1
            while(vertice1.padre != -1){
                camino1.push_back(vertice1.id); // pusheo el vertice que esta en el camino
                vertice1 = G.vertices[G.vertices[vertice1.id].padre]; // Agarro el padre y sigo iterando
            }
            camino1.push_back(vertice1.id);
            //armo el camino2
            while(vertice2.padre != -1){
                camino2.push_back(vertice2.id); // pusheo el vertice que esta en el camino
                vertice2 = G.vertices[G.vertices[vertice2.id].padre]; // Agarro el padre y sigo iterando
            }
            camino2.push_back(vertice2.id);

            salida.push_back(camino1);
            salida.push_back(camino2);
        }else{
            salida.push_back(res);
        }
    }



    /*ofstream fw("/home/tobi/Escritorio/facultad/Algo 3/algo3-tp2/res.txt");
    if(fw.is_open()){
        for (int i = 0; i < salida.size(); i++){
            for (int j = 0; j < salida[i].size(); j++)
            {
                fw << salida[i][j];
            }
            fw << "\n";
        }
    }*/

    // Para printear la salida como pide el enunciado
    for(auto linea : salida){
        for(auto e : linea){
            cout << e << " ";
        }
        cout << endl;
    }
    //auto t_end = std::chrono::high_resolution_clock::now();
    //double elapsed_time_ms = std::chrono::duration<double, std::milli>(t_end-t_start).count();
    //cout << "\n segundos:" << elapsed_time_ms / 1000 ;
}






