#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <chrono>
#include <iomanip>
using namespace std;

int Mfilas, Ncolumnas;
int filaActual;
string path;
vector<vector<int>> matriz(2);
ifstream stream;

vector<int> padres; // La posición i dice quién es el padre del iésimo elemento de las dos líneas
                    // que vamos a estar mirando
vector<int> yaRearme; // Para saber si esa raíz es un elemento de la fila nueva o si es vieja

int arbolesBorrados = 0;

void parsearSiguienteFila(){
    for(int i = 0; i < Ncolumnas; i++){
        if(padres[i] == i) arbolesBorrados++; // Se borra una componente conexa por cada raíz que queda en la primer fila
    }

    // Observación: cuando avanzamos de fila, todos los elementos de padres que corresponden a matriz[1] tenemos que
    // pasarlo a su lugar correspondiente a matriz[0] y restarles N a menos que su padre sea menor que N
    // en cuyo caso no hay que restarle nada
    for(int i = 0; i < Ncolumnas; i++){
        if(padres[Ncolumnas+i] >= Ncolumnas){
           padres[i] = padres[Ncolumnas+i] - Ncolumnas;
        }
        else padres[i] = padres[Ncolumnas+i];
    }
    for(int i = Ncolumnas; i < 2*Ncolumnas; i++){ // Ponemos en -1 los padres de la nueva fila
        padres[i] = -1;
    }
    matriz[0] = matriz[1];
    int temp;
    for (int i= 0; i< Ncolumnas; i++){ // Cargamos nueva fila
        stream >> temp;
        matriz[1][i] = temp;
    }
    filaActual++;

    for(int e : yaRearme){
        e = 0;
    }
}

int find(int i){ //devuelve la raíz del árbol del elemento i. No lo hacemos recursivo para que no reviente el stack
    if (padres[i]==-1){
        return -1;
    }
    int raiz = i;
    while (padres[raiz] != raiz){ // Encontrar la raíz
        raiz = padres[raiz];
    }
    while(padres[i] != raiz){ // Actualizamos los padres de los elementos del camino para que apunten directo a la raíz
        int padreDeI = padres[i];
        padres[i] = raiz;
        i = padreDeI;
    }
    return raiz;
}

void makeSet(int i){
    padres[i] = i;
}

void unir(int izq, int arriba){ // IMPORTANTE: engancha el árbol de k debajo del árbol de i
    padres[find(arriba)] = find(izq);
}




int main(int argc, char *argv[]) {
    auto t_start = std::chrono::high_resolution_clock::now();
    std::ios::sync_with_stdio(false);
    std::cin.tie(0);
    if (argc < 1){
        cout << "Provea un archivo como argumento de entrada \n Uso: ej2 [filename]\n";
        exit(0);
    }
    path = argv[1];
    stream.open(path);
    if (!stream.is_open()){
        cout << "Error abriendo el archivo " << path << endl;

        exit(1);
    }
    stream >> Mfilas >> Ncolumnas;
    filaActual = 0;
    matriz[0].resize(Ncolumnas);
    matriz[1].resize(Ncolumnas);
    padres.resize(2*Ncolumnas, -1);
    yaRearme.resize(2*Ncolumnas, 0);

    if (Mfilas == 1){
        parsearSiguienteFila();
        for(int i = 0; i < Ncolumnas; i++){ // Agregamos los elementos al union find
            if(matriz[1][i] == 0) continue;
            if(i==0){
                padres[0] = 0;
                continue;
            }
            if(find(i-1) != -1){
                padres[i] = find(i-1);
            }
            else padres[i] = i;
        }
    }
    else if(Mfilas == 0){
        cout << 0 << endl;
        return 0;
    }

    for(int fila = filaActual; fila < Mfilas; fila++){
        parsearSiguienteFila();
        for(int indiceMatriz=0; indiceMatriz < Ncolumnas; indiceMatriz++){
            int elemento = indiceMatriz+Ncolumnas; //porque estamos en la segunda fila de la matriz
            if(matriz[1][indiceMatriz] == 0) continue;
            if(indiceMatriz==0){
                if(matriz[0][0] == 1) padres[Ncolumnas] = find(padres[0]);
                else padres[Ncolumnas] = Ncolumnas;
                yaRearme[Ncolumnas] = 1;
            }
            if(matriz[0][indiceMatriz] == 1 && matriz[1][indiceMatriz-1] == 1){ //En este caso hay que unir ambos árboles y luego agregar i
                unir(elemento-1, indiceMatriz);
                if(yaRearme[find(elemento-1)]) padres[elemento] = find(elemento-1);
                else{ // Tengo que poner al nuevo elemento como raíz
                    makeSet(elemento);
                    padres[find(elemento-1)] = elemento;
                    yaRearme[elemento] = 1;
                }
                continue;
            }
            else if(matriz[1][indiceMatriz-1] == 1){ // En este caso sólo hay que agregar i al árbol de la izquierda
                if(yaRearme[find(elemento-1)]) padres[elemento] = find(elemento-1);
                else{ // Tengo que poner al nuevo elemento como raíz
                    makeSet(elemento);
                    padres[find(elemento-1)] = elemento;
                    yaRearme[elemento] = 1;
                }
                continue;
            }
            else if(matriz[0][indiceMatriz] == 1) { // En este caso sólo hay que agregar i al árbol de arriba
                if(yaRearme[find(indiceMatriz)]) padres[elemento] = find(indiceMatriz);
                else{ // Tengo que poner al nuevo elemento como raíz
                    makeSet(elemento);
                    padres[find(indiceMatriz)] = elemento;
                    yaRearme[elemento] = 1;
                }
                continue;
            }
            else{ // En este caso ni arriba ni a la izquierda hay 1
                makeSet(elemento);
                yaRearme[elemento] = 1;
            }
        }
    }
    // En este punto ya parseamos todas las filas de la matriz. Sólo resta contar la cantidad de árboles (componentes conexas)
    int cantArboles = 0;
    for(int i = 0; i < 2*Ncolumnas; i++){
        if(padres[i] == i) cantArboles++;
    }

    cout << cantArboles + arbolesBorrados << "\n";

    // CLOCK
    //auto t_end = std::chrono::high_resolution_clock::now();
    //double elapsed_time_ms = std::chrono::duration<double, std::milli>(t_end-t_start).count();
    //cout <<setprecision(3) << elapsed_time_ms / 1000 ;

    return 0;


}
