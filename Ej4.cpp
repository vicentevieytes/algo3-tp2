#include <iostream>
#include <vector>
#include <string>
#include <list>
#include <bits/stdc++.h>
#include <chrono>
using namespace std;



struct intervalo{ // un intervalo [a, b]. c es 1 si es un intervalo contenido y 0 si no esta contenido en ningun otro
    int a, b ,c;
};

bool compare(intervalo i1, intervalo i2){
    return (i1.a < i2.a);
}

bool igualdadIntervalos(intervalo i1, intervalo i2){
    return ( (i1.a==i2.a) && (i1.b==i2.b) );
}

struct vertices_cdt{
    int d, pi, id;
};

struct grafo {
    vector<list<vector<int>>> adj; //  En el vector que representa una arista tenemos 3 posiciones, la primera es a qué
    vector<vertices_cdt> vertices; //  nodo apunta; la segunda, cuanto pesa y la tercera, qué tipo de arista es.
};                                 //  tipo 0: arista B, tipo 1: arista C y tipo 2 es arista in-out

int n;
vector<intervalo> I;
string path;
ifstream stream;


vector<intervalo> removerContenidos(vector<intervalo> &I){
    vector<intervalo> sin_contenidos;
    for(int j=0;j<I.size();j++){
        bool no_es_un_contenido = true;
        for(int k=0; k<I.size();k++){
            if(I[k].a <= I[j].a && I[k].b >= I[j].b && j!=k){
                no_es_un_contenido = false;
            }
        }
        if(no_es_un_contenido){
            I[j].c = 0;
            sin_contenidos.push_back(I[j]);
        }else{
            I[j].c = 1;
        }
    }
    return sin_contenidos;
}

grafo crear_grafo_cdt (vector<intervalo> I,vector<intervalo> sin_contenidos){
    grafo grafo_cdt;
    list<vector<int>> lista_vacia;

    grafo_cdt.adj.push_back(lista_vacia); // defino el nodo 0 del grafo
    for(int i=0;i<sin_contenidos.size();i++){ // agrego los nodos del medio del grafo
        grafo_cdt.adj.push_back(lista_vacia);
    }
    grafo_cdt.adj.push_back(lista_vacia); // defino el nodo n+1

    // en este momento nuestro grafo deberia tener O(I.size()+2) nodos
    // pongo aristas B O(n^2)
    vector<int> dummy = {0,1,0};
    for(int i=0;i<sin_contenidos.size();i++){
        for(int j=i;j<sin_contenidos.size();j++){
            if(sin_contenidos[i].b>sin_contenidos[j].a && i!=j){
                dummy[0] = j+1; // por ahi se rompe
                grafo_cdt.adj[i+1].push_back(dummy); // i+1 y j+1 porque el primero es el nodo 0
            }
        }
    }
    // pongo aristas C O(n^2)
    // voy a pushear el intervalo n+1 al vector de intervalos para conectarlo
    intervalo ultimo;
    ultimo.a = INT32_MAX-100;
    ultimo.b = INT32_MAX-50;
    ultimo.c = 0;
    I.push_back(ultimo);
    dummy = {0,0,1};
    // pongo aristas C para el nodo 0
    int cierra_mas_temprano = I[0].b;
    int indiceEnSinIntervalos = 1;
    for(int i=0;i<I.size();i++){
        if(I[i].a < cierra_mas_temprano && I[i].c == 0){
            dummy[0] = indiceEnSinIntervalos;
            grafo_cdt.adj[0].push_back(dummy);
        }
        if(I[i].b < cierra_mas_temprano){
            cierra_mas_temprano = I[i].b;
        }
        if(I[i].c == 0) indiceEnSinIntervalos++;
    }
    // pongo aristas C para el resto de nodos. Es decir todos menos el 0 que ya le puse y el n+1 que nunca tiene
    dummy = {0,1,1};
    for(int i=0;i<sin_contenidos.size();i++){
        intervalo actual = sin_contenidos[i];
        int siguiente;
        int aAgregar = i; // para llevar la cuenta del índice en sin_contenidos del intervalo a cual vamos a conectar con i
        for(int k=0;k<I.size();k++){ // busco el índice del siguiente no solapado al intervalo i en el vector I
            if(actual.b < I[k].a && actual.b < I[k].b){
                siguiente = k;
                break;
            }
            if(I[k].a > actual.a && I[k].c == 0) aAgregar++; // para llevar la cuenta del índice en sin_contenidos
                                                            //  del intervalo a cual vamos a conectar con i
            if(k == I.size()-1) siguiente = I.size()-1;
        }
        cierra_mas_temprano = I[siguiente].b;
        for(int j=siguiente;j<I.size();j++){
            if(I[j].c == 0) aAgregar++;
            if(I[j].a > sin_contenidos[i].b && I[j].a < cierra_mas_temprano && I[j].c == 0){
                dummy[0] = aAgregar + 1; //+1 para saltearnos el nodo 0
                grafo_cdt.adj[i+1].push_back(dummy); //i+1 para saltearnos el nodo 0
            }
            if(I[j].b<cierra_mas_temprano){
                cierra_mas_temprano = I[j].b;
            }
        }
    }
    //creo el nuevo grafo con los nodos inout
    grafo grafo_cdt_inout;
    for(auto &e : grafo_cdt.adj[0]){ // multipico por dos los del nodo 0
        e[0] = 2 * e[0];
    }
    grafo_cdt_inout.adj.push_back(grafo_cdt.adj[0]); // pusheo el nodo 0
    dummy = {0,0,2};
    for(int i=1;i<grafo_cdt.adj.size()-1;i++) { // pusheo el resto de los vértices
        grafo_cdt_inout.adj.push_back(grafo_cdt.adj[i]); // posicion i (los nodos in)
        grafo_cdt_inout.adj.push_back(lista_vacia);// posicion i+1 (los nodos out)
        dummy[0] = 2*i;
        grafo_cdt_inout.adj[(2 * i) - 1].push_back(dummy); // conecto los dos nodos. Le pongo 2*i porque en cada iteración
    }                                               // estamos agregando dos nodos
    grafo_cdt_inout.adj.push_back(grafo_cdt.adj[grafo_cdt.adj.size()-1]); // pusheo el nodo n+1
    for(vector<int> arista : grafo_cdt_inout.adj[0]){  // arreglo las aristas del nodo 0
        //vector<int> arista = *it;
        //arista[0] = 2*arista[0]; // hago que las aristas viejas apunten a los nodos out
        arista[0] = 2 * (arista)[0];
    }
    for(int i=1;i<grafo_cdt_inout.adj.size();i++){ // arreglo las aristas de los nodos in que ahora contienen a todas las aristas
        vector<vector<int>> aristasAborrar;
        for(vector<int> &arista : grafo_cdt_inout.adj[i]){
            if(arista[2] == 0){ //es arista B
                vector<int> nuevaArista = arista;
                nuevaArista[0] = ( 2* (arista[0]) ) - 1; // apunta a un nodo in
                grafo_cdt_inout.adj[i+1].push_back((nuevaArista)); // sale de un nodo out
                aristasAborrar.push_back(arista);
            }else if(arista[2] == 1){ // es arista C
                if(arista[0]!=grafo_cdt.adj.size()-1)arista[0] = 2 * arista[0]; //si no va al nodo n+1
                else arista[0] = (2 * arista[0]) - 1; // en caso de que vaya al nodo n+1

            }
        }
        for(const vector<int>& arista : aristasAborrar){
            grafo_cdt_inout.adj[i].remove(arista);
        }
        i++; // i aumenta de a 2 para solo mirar los nodos in
    }
    return grafo_cdt_inout;
}

void inicializar_vertices_camino_minimo(grafo &grafo_ctd){
    for(int i=0;i<grafo_ctd.adj.size();i++){
        vertices_cdt vertice_vacio;
        vertice_vacio.d = INT32_MAX/2;
        vertice_vacio.pi = -1;
        vertice_vacio.id = i;
        grafo_ctd.vertices.push_back(vertice_vacio);
    }
    grafo_ctd.vertices[0].d = 0;
}

void relajar (vertices_cdt &u, vertices_cdt &v,int w){
    if(v.d > u.d + w){
        v.d = u.d + w;
        v.pi = u.id;
    }
}

vector<intervalo> camino_minimo_ctd (grafo grafo_ctd,vector<intervalo> sin_contenidos){
    // como el vector de intervalos ya esta ordenado topologicamente la primera parte del algiritmo nos la salteamos
    // lleno el vector de vertices usando la lista de adyacencia
    inicializar_vertices_camino_minimo(grafo_ctd);
    for (int i=0;i<grafo_ctd.adj.size();i++){
        for(vector<int> arista : grafo_ctd.adj[i]){
            relajar(grafo_ctd.vertices[i],grafo_ctd.vertices[arista[0]],arista[1]);
        }
    }
    vector<intervalo> result;
    //cout << grafo_ctd.vertices[grafo_ctd.vertices.size()-1].pi << endl;
    int i = grafo_ctd.vertices.size()-1;
    while(i>0){ // reconstruyo el camino mirando los padres del vertice n+1
        i = grafo_ctd.vertices[i].pi;
        if(i<=0) break;
        result.push_back(sin_contenidos[((i+1) / 2)-1]);
    }
    if(result.empty())result.push_back(sin_contenidos[0]); //si no hay ningún camino es porque había sólo un elemento no contenido
    return result;
}

vector<intervalo> ctd (vector<intervalo> I){
    sort(I.begin(), I.end(), compare);

    vector<intervalo> sin_contenidos = removerContenidos(I);// me construyo un vector con los intervalos que no estan contenidos dentro de otro O(n^2)
    grafo grafo_ctd = crear_grafo_cdt(I,sin_contenidos);
    vector<intervalo> result = camino_minimo_ctd(grafo_ctd,sin_contenidos);
    return result;
}

bool verificarSolucion(vector<intervalo> solucion){
    for(intervalo eI : I){
        bool esDominado = false;
        for(intervalo eS : solucion){
            if((eI.a < eS.a && eI.b > eS.a) || (eS.a < eI.a && eS.b > eI.a) || (eI.a < eS.a && eI.b > eS.b) || (eS.a < eI.a && eS.b > eI.b)){
                esDominado = true; //Por descrip de instancia, no pueden ser iguales y los extremos son todos distintos
                break;
            }
        }
        if(not esDominado) return false;
    }
    return true;
}


int main(int argc, char *argv[]) {
    auto t_start = std::chrono::high_resolution_clock::now();
    if (argc < 1){
        cout << "Provea un archivo como argumento de entrada \n Uso: ej2 [filename]\n";
        exit(0);
    }
    path = argv[1];
    stream.open(path);
    if (!stream.is_open()){
        cout << "Error abriendo el archivo " << path << endl;

        exit(1);
    }
    stream >> n;

    for (int i= 0; i< n; i++){
        intervalo x{};
        stream >> x.a >> x.b;
        I.push_back(x);
    }
    vector<intervalo> result = ctd(I);
    vector<int> output; //queremos los índices de esos intervalos
    for(int i=0; i<result.size();i++){
        for(int j=0; j<I.size();j++){
            if(result[i].a == I[j].a && result[i].b == I[j].b){
                if(output.size() > 0){
                    if(output[output.size()-1]!=j)output.push_back(j); //no ponemos repetidos
                }
                else output.push_back(j);
                break;
            }
        }
    }
    if(output.size()==1){ // Caso borde en el que un intervalo domina al resto
        output.push_back((output[0]+1) % I.size() ); //Le agregamos cualquier otro
        for(intervalo interv : I) {// Idem para result, lo vamos a usar en el verificador
            if(not igualdadIntervalos(interv, result[0])){
                result.push_back(interv);
                break;
            }
        }
    }
    sort(output.begin(), output.end());

    unique(result.begin(), result.end(), igualdadIntervalos); //elimino repetidos de result para usarlo en el verificador

    // Printeo la salida como se pide
    cout << output.size() << endl;
    for(int e : output){
        cout << e << " ";
    }
    cout << endl;

    // CLOCK
    //auto t_end = std::chrono::high_resolution_clock::now();
    //double elapsed_time_ms = std::chrono::duration<double, std::milli>(t_end-t_start).count();
    //cout << "Milisegundos:" << elapsed_time_ms / 1 << endl ;

    //Verifica que el resultado sea efectivamente CDT, aunque no da garantías de si efectivamente es el mínimo
    //if(verificarSolucion(result)) cout << "El resultado es CDT" << endl;
    //else cout << "La solución no es correcta" << endl;

    return 0;
}
