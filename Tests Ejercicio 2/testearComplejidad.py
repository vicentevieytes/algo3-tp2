#!/usr/bin/env python3
#Script para correr los test de complejidad

import os
import subprocess
import sys
from tabulate import tabulate

pathEjecutable = sys.argv[1]
pathDirectorioTests = sys.argv[2]

tabla = []
for n in range(1,10):
    fila = [f'$10^{n}$']
    for p in range (1, 10,1):
        pathTest = f'{pathDirectorioTests}/n{n}_p0,{p}'
        print(f'Testeando con n{n}_p0,{p}.in')
        resultado = subprocess.getoutput(f'{pathEjecutable} {pathTest}.in')
        print(resultado)
        resLista = resultado.splitlines()
        resValor = resLista[0].strip(" \n")
        resTiempo = resLista[1].strip(" \n")
        with open(f'{pathTest}.out') as f:
            test = f.read()
        if (int(test) != int(resValor)):
            print(f"test es {test} y resValor es {resValor}")
            print(f"NOT PASSED")
            exit()
        print("PASSED")
        print(f"resTiempo es {resTiempo}")
        fila.append(resTiempo)
    tabla.append(fila)
print("ALL TESTS PASSED")

for j in range(1,10):
    promedio = 0.0
    for i in range(1,10):
        promedio += float(fila[i])
    promedio = promedio / 9
    tabla[j].append(promedio)
    

fout = open("latexTable.txt", "w")
foutplain = open("plainTable.txt", "w")

fout.write(tabulate(tabla,["Tamaño", "p=0.1", "p=0.2", "p=0.3", "p=0.4", "p=0.5","p=0.6","p=0.7", "p=0.8", "p=0.9", "Promedio"], tablefmt="latex_raw"))
foutplain.write(tabulate(tabla,["Tamaño", "p=0.1", "p=0.2", "p=0.3", "p=0.4", "p=0.5","p=0.6","p=0.7", "p=0.8", "p=0.9", "Promedio"], tablefmt="plain"))


        

        

        

