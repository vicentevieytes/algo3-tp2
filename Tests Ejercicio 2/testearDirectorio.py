#!/usr/bin/env python3
#Script para correr los tests de un directorio

import os
import sys
import subprocess
#Devuelve un diccionario {nombreTest: [tiempo, resultado]}
def testearDirectorio(executable, directory):
    res = {}
    testCases = {}
    for filename in os.listdir(directory):
        if filename.endswith(".in"):
            testCases[filename] = filename.replace(".in", ".out")
    for testCase in testCases:
        print(f'Testing {testCase}')
        result = subprocess.getoutput(f'{executable} {directory}/{testCase}')
        resultList = result.splitlines()
        resultValue = resultList[0].strip(" \n")
        resultTime = resultList[1].strip(" \n")
        with open(f'{directory}/{testCases[testCase]}') as f:
            test = f.read()
        if (int(test) != int(resultValue)):
            print(f"test es {test} y resValor es {resultValue}")
            print(f"NOT PASSED")
            exit()
        print("PASSED")
        print(f"resTiempo es {resultTime}")
        res[testCase.strip(".in")] = [resultValue, resultTime]
       
    return res
    

if __name__ == '__main__':
    executable = sys.argv[1]
    directory = sys.argv[2]
    testearDirectorio(executable, directory)