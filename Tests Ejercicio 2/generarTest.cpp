#include <iostream>
#include <string>
#include <vector>
#include <random>
#include <fstream>
#include <queue>
#include <array>
using namespace std;

struct nodoVisitado{
    nodoVisitado(int v){
        visitado = false;
        valor = v;
    }
    bool visitado;
    int valor;
};

typedef array<int,2> posicion; 
typedef vector<vector<nodoVisitado>> matriz;

int cantFilas, cantColumnas;



vector<posicion> adyacentes(matriz& M, posicion p){
    vector<posicion> adj;
    int f= p[0] ;
    int c = p[1];
    if (c > 0){
        if (M[f][c-1].valor == 1) adj.push_back({f, c-1});
    }
    if (c < cantColumnas-1){
        if (M[f][c+1].valor == 1) adj.push_back({f, c+1});
    }
    if (f > 0){
        if (M[f-1][c].valor == 1) adj.push_back({f-1, c});
    }
    if (f < cantFilas-1){
        if (M[f+1][c].valor == 1) adj.push_back({f+1, c});
    }
    return adj;
}

void bfs(matriz &matriz, posicion pos){
    queue<posicion> queue;
    queue.push(pos);

    while (queue.size() > 0){
        posicion p = queue.front();
        queue.pop();
        for (auto u : adyacentes(matriz, p)){
            if (matriz[u[0]][u[1]].visitado == false){
                matriz[u[0]][u[1]].visitado = true;
                queue.push(u);
            }
        }
        matriz[p[0]][p[1]].visitado = true;
    }
}

int contarComponentes(matriz& matriz){
  //  cout << "Contando componentes conexas...\n";
    int componentes = 0; 
    for (int i= 0; i< cantFilas; i++ ){
        for (int j=0; j< cantColumnas; j++){
            if (matriz[i][j].valor== 1 && !matriz[i][j].visitado){
                bfs(matriz, {i, j});
                componentes++;
            }
        }
    }
    return componentes;
}

int main(int argc,char* argv[]){
    /*
    if (argc != 5){
        cout << "Uso: \"./generador.py\" o \"py generador.py\" <cantFilas> <cantColumnas> <probabilidad> <nombreArchivo>";
        return 1;
    }
    */
    cantFilas = atoi(argv[1]);
    cantColumnas = atoi(argv[2]);
    float probabilidad = stod(argv[3]);
    string nombreArchivo = argv[4];

    std::random_device rd;
    //std::seed_seq seed (strParaSeed.begin(), strParaSeed.end()); 
    std::default_random_engine generator(rd()); 
    std::bernoulli_distribution distribution(probabilidad);

    matriz matriz = {};

    ofstream f(nombreArchivo);
//    cout<< "Escribiendo a " << nombreArchivo << "...\n";
    f << cantFilas << " " << cantColumnas << " \n";
    for (int i=0; i < cantFilas; i++){
            vector<nodoVisitado> fila = {};
            matriz.push_back(fila);
        for (int j=0; j < cantColumnas; j++){
            int valorOut = distribution(generator);
            nodoVisitado valorMatriz(valorOut);
            matriz[i].push_back(valorMatriz);
            f << valorOut << " ";
        }
        f << "\n";
    }
    //cout << "Hecho.\n";
    int componentes = contarComponentes(matriz);
    cout << componentes << "\n";
    return 0;
}