#generate random test cases using Ej2 executable
from imghdr import tests
import random
import os
import sys
import subprocess
from tabulate import tabulate
from testearDirectorio import testearDirectorio
import matplotlib.pyplot as plt

exe = "./Ej2"
directory = "testsRandom"
os.system(f"rm -r {directory}")
os.system(f"mkdir {directory}")
testSize = {}
tabla = []
maxN = 0
maxM =0
maxP = 0.0
for i in range(1,150):
    n = random.randint(1,1000)
    if n > maxN:
        maxN = n
    m = random.randint(1,1000)
    if m > maxM:
        maxM = m
    p = random.randint(1,100)/100
    if p > maxP:
        maxP = p
    nombreTest = f'Random_{i}'
    testSize[nombreTest] = n*m
    tabla.append([nombreTest, p, m, n, n*m]) 
    comando = f'./generarTests {m} {n} {p} {directory}/{nombreTest}.in > {directory}/{nombreTest}.out'
    print(comando)
    print(f'generando {nombreTest}, returns... {os.system(comando)}') 
results = testearDirectorio(exe, directory)
print (f'maxN: {maxN}, maxM: {maxM}, maxP: {maxP}')

for fila in tabla:
    fila.append(results[fila[0]][0])
    fila.append(results[fila[0]][1])

tabla.sort(key=lambda x: x[6], reverse=True)

tablaout = tabulate(tabla, headers=['Test', 'p', 'M', 'N', 'N*M', 'Resultado','Tiempo'], tablefmt='latex_longtable')
tablaplainout  = tabulate(tabla, headers=['Test', 'p', 'M', 'N', 'N*M', 'Resultado','Tiempo'], tablefmt='plain')

f = open('tablaRandoms.tex', 'w')
f.write(tablaout)
f.close()

f = open('tablaRandomsPlain.txt', 'w')
f.write(tablaplainout)
f.close()


diccPlot = {}
for test in results:
    diccPlot[int(testSize[test])] = float(results[test][1])
#bar plot test time over test size
plt.scatter(list(diccPlot.keys()), list(diccPlot.values()), s=1, alpha=1)
plt.xlabel('Tamaño de test')
plt.ylabel('Tiempo de ejecución')
plt.title('Tiempo de ejecución de testsRandom')
plt.savefig('tiempoTestsRandom.png')
plt.show()














