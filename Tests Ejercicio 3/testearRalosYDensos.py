import subprocess
import sys
import os
from tabulate import tabulate
import matplotlib.pyplot as plt
import numpy as np

def checkearCiclo(ciclo, graph):
    actual = ciclo[0]
    suma = 0
    res = True
    for i in range(len(ciclo)):
        if(graph[actual][ciclo[i]] == "INF"):
            res = False
            break
        indiceSIguiente = (i+1) % len(ciclo)
        suma += graph[actual][ciclo[indiceSIguiente]]
        actual = ciclo[indiceSIguiente]    
    return res and suma < 0

def testearUnTest(directorio, testCase):
    file = open(f"{directorio}/{testCase}.in", "r")
    lines = file.readlines()
    vertices = int(lines[0].split()[0])
    edges = int(lines[0].split()[1])
    verticesPorTest[testCase] = vertices
    edgesPorTest[testCase] = edges
    del lines[0]

    fila = ["INF"] * vertices
    graph=[]
    for i in range(vertices):
        graph.append(fila.copy())
    for line in lines:
        if line == "":
            continue
        u, v, w = line.split()
        u = int(u)
        v = int(v)
        w = int(w)
        graph[u][v] = w
    for i in range(vertices):
        graph[i][i] = 0
    file.close()
    res = subprocess.getoutput(f"./Ej3 {directorio}/{testCase}.in")
    res = res.splitlines()
    tiempo = float(res[0])
    tiempoPorTest[testCase] = tiempo
    res.remove(res[0])
    print(res[0])
    if int(res[0]) == 0:
        ciclo = list(map(int, res[1].split()))
        if(checkearCiclo(ciclo, graph)):
            print(f"{testCase} OK ciclo detectado, tiempo {tiempo}")
            cicloDetectado[testCase] = "SI"
    else:
        testCaseRes = open(f"{directorio}/{testCase}.out", "r")
        testCaseRes = testCaseRes.readlines()
        for i in range(len(testCaseRes)):
            testCaseRes[i] = testCaseRes[i].strip()
        for i in range(len(res)):
            res[i] = res[i].strip()
        
        if testCaseRes == res:
            print(f"{testCase} OK, tiempo {tiempo}")
            cicloDetectado[testCase] = "NO"

cicloDetectado = {}    
tiempoPorTest = {}
tamañoPorTest = {}
verticesPorTest = {}
edgesPorTest = {}

directorio = sys.argv[1]
for archivo in os.listdir(directorio):
    if archivo.endswith(".in"):
        print(f"voyATestear {archivo}")
        testearUnTest(directorio, archivo.strip(".in"))

tabla = []
for testCase in tiempoPorTest:
    fila = [testCase, verticesPorTest[testCase], edgesPorTest[testCase], cicloDetectado[testCase], tiempoPorTest[testCase]]
    tabla.append(fila)
tabla = sorted(tabla, key=lambda x: x[4])
tablaout = tabulate(tabla, headers=["TestCase", "Vertices", "Aristas", "Ciclo Neg", "Tiempo"], tablefmt="latex")
tablaplain = tabulate(tabla, headers=["TestCase", "Vertices", "Aristas", "Ciclo Neg" ,"Tiempo"], tablefmt="plain")
tablafile = open("tablaRalosConNegativos.tex", "w")
tablafileplain = open("tablaRalosConNegativos.txt", "w")
tablafileplain.write(tablaplain)
tablafile.write(tablaout)
tablafile.close()
tablafileplain.close()

Ralos = [list(verticesPorTest.values()), list(tiempoPorTest.values())]

cicloDetectado = {}    
tiempoPorTest = {}
tamañoPorTest = {}
verticesPorTest = {}
edgesPorTest = {}
directorio = sys.argv[2]
for archivo in os.listdir(directorio):
    if archivo.endswith(".in"):
        print(f"voyATestear {archivo}")
        testearUnTest(directorio, archivo.strip(".in"))

tabla = []
for testCase in tiempoPorTest:
    fila = [testCase, verticesPorTest[testCase], edgesPorTest[testCase], cicloDetectado[testCase], tiempoPorTest[testCase]]
    tabla.append(fila)
tabla = sorted(tabla, key=lambda x: x[4])
tablaout = tabulate(tabla, headers=["TestCase", "Vertices", "Aristas", "Ciclo Neg" ,"Tiempo"], tablefmt="latex")
tablaplain = tabulate(tabla, headers=["TestCase", "Vertices", "Aristas","Ciclo Neg", "Tiempo"], tablefmt="plain")
tablafile = open("tablaDensosConNegativos.tex", "w")
tablafileplain = open("tablaDensosConNegativos.txt", "w")
tablafileplain.write(tablaplain)
tablafile.write(tablaout)
tablafile.close()
tablafileplain.close()

densos = [list(verticesPorTest.values()), list(tiempoPorTest.values())]

#plot double bar graph with densos and ralos
plt.figure(figsize=(10,10))
plt.title("Tiempo de ejecucion de los casos de prueba")
plt.xlabel("Tamaño del grafo")
plt.ylabel("Tiempo de ejecucion")
plt.xlim([100, 500])
plt.scatter(densos[0], densos[1], label="Densos")
plt.scatter(Ralos[0],Ralos[1], label="Ralos")
plt.legend()
plt.savefig("tiempoConNegativos.png")
ax = plt.gca()
ax.set_yscale("log")
plt.savefig("tiempoLogConNegativos.png")
plt.show()
