#generate sparse graphs for testing
import os
import sys
import random
import tabulate as tb
import matplotlib as plt
from gen_graph import main

for i in range (1,25):
    n = random.randint(10, 500)
    m = random.randint(n, n*4)
    testname = f'ralo_{i}_n{n}_m{m}'
    main(['-grnm', '--directed', '-n', str(n), '-m', str(m), '-w', 'int', '-wmin', '0', '-wmax', '20', '--output', f'testsRalos/{testname}.in'])
    command = f'python floydWarshall.py testsRalos/{testname}.in > testsRalos/{testname}.out'
    os.system(command)
    print(f'{testname} generated')

for i in range (1,25):
    n = random.randint(10,500)
    m = random.randint(int(n**2/4), int(n**2/2))
    testname = f'denso_{i}_n{n}_m{m}'

    main(['-grnm', '--directed', '-n', str(n), '-m', str(m), '-w', 'int', '-wmin', '0', '-wmax', '20', '--output', f'testsDensos/{testname}.in'])
    command = f'python floydWarshall.py testsDensos/{testname}.in > testsDensos/{testname}.out'
    os.system(command)
    print(f'{testname} generated')

