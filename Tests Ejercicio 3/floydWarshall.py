import sys
# Python Program for Floyd Warshall Algorithm
# Number of vertices in the graph

 
# Define infinity as the large
# enough value. This value will be
# used for vertices not connected to each other

 
# Solves all pair shortest path
# via Floyd Warshall Algorithm
def sum(a,b):
    if a == "INF" or b == "INF":
        return "INF"
    return a+b
def cmp(a,b):
    if b == "INF":
        return False
    if a == "INF":
        return True
    return a > b
def floydWarshall(graph, V):
    dist = list(map(lambda i: list(map(lambda j: j, i)), graph))
    for k in range(V):
        # pick all vertices as source one by one
        for i in range(V):
            # Pick all vertices as destination for the
            # above picked source
            for j in range(V):
                # If vertex k is on the shortest path from
                # i to j, then update the value of dist[i][j]
                if  cmp(dist[i][j], sum(dist[i][k], dist[k][j])):
                    dist[i][j] = sum(dist[i][k], dist[k][j])
    for i in range(V):
        if (dist[i][i] < 0):
            print("0")
            return
    printSolution(dist, V)
    return

# A utility function to print the solution
def printSolution(dist, V):
    print ("1")
    for i in range(V):
        for j in range(V):
            if(dist[i][j] == "INF"):
                print ("INF",end=" ")
            else:
                print (dist[i][j],end=' ')
            if j == V-1:
                print ()

if __name__ == '__main__':

    #parse graph from edge list
    file = open(sys.argv[1], "r")
    lines = file.readlines()
    vertices = int(lines[0].split()[0])
    edges = int(lines[0].split()[1])
    del lines[0]
    
    fila = ["INF"] * vertices
    graph=[]
    for i in range(vertices):
        graph.append(fila.copy())
    for line in lines:
        if line == "":
            continue
        u, v, w = line.split()
        u = int(u)
        v = int(v)
        w = int(w)
        graph[u][v] = w
    for i in range(vertices):
        graph[i][i] = 0
# Print the solution
    floydWarshall(graph, vertices)
# This code is contributed by Mythri J L