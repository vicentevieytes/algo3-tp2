#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <queue>
#include <climits>
#include <algorithm>

using namespace std;
bool flag = false;
vector<vector<pair<int, int>>> G;

vector<int> bellmanFord(int n, const vector<vector<int>>& edges){
    int m = edges.size();
    vector<int> d(n);
    vector<int> p(n, -1);
    int x;
    for (int i = 0; i < n; ++i) {
        x = -1;
        for (auto e : edges) {
            int u = e[0];
            int v = e[1];
            int weight = e[2];
            if (d[u] + weight < d[v]) {
                d[v] = d[u] + weight;
                p[v] = u;
                x = v;
            }
        }
    }

    if (x == -1) {
        ;
    } else {
        flag = true;
        for (int i = 0; i < n; ++i)
            x = p[x];
        vector<int> cycle;
        for (int v = x;; v = p[v]) {
            cycle.push_back(v);
            if (v == x && cycle.size() > 1)
                break;
        }
        reverse(cycle.begin(), cycle.end());
        cout << "0\n";
        for (int v : cycle)
            cout << v << ' ';
        cout << endl;
    
    }
    return d;
}

void dijkstra(int src, vector<vector<int>> &path, int &N){

    priority_queue<pair<int, int>> cola;
    vector<int> visitados(N);
    path[src][src] = 0;
    cola.push({0, src});
    while (!cola.empty()){
        int f = cola.top().second;
        cola.pop();
        if(!visitados[f]){
            visitados[f] = true;
            for (auto u : G[f]){
                int s = u.first;
                int w = u.second;
                if(path[src][f] + w < path[src][s]){
                    path[src][s] = path[src][f] + w;
                    cola.push({-path[src][s], s});
                }
            }
        }
    }
}
int main(int argc, char *argv[]){

    if (argc < 1){
        cout << "Provea un archivo como argumento de entrada \n Uso: ./ej3 [filename]\n";
        exit(0);
    }
    string path;
    ifstream stream;

    path = argv[1];
    stream.open(path);

    if (!stream.is_open()){
        cout << "Error abriendo el archivo " << path << endl;

        exit(1);
    }
    int N, E;

    stream >> N >> E;
    vector<vector<int>> edges;

    for (int i = 0; i < E; i++){
        int u, v, w;
        stream >> u >> v >> w; 
        edges.push_back({u,v,w});
    }
    stream.close();
    vector<int> h = bellmanFord(N, edges);
    if (flag){return 0;}
  // Modificamos los pesos
    for (int i = 0; i < E; i++){
        int u = edges[i][0];
        int v = edges[i][1];
        edges[i][2] += (h[u] - h[v]);
    }
    //G es la lista de adyacencias, g[0].first es el vertice, g[0].second es el peso
    for (int i = 0; i< N; i++){
        G.push_back(vector<pair<int, int>>());
    }
    for (auto e : edges){
        G[e[0]].push_back({e[1], e[2]});
    }
   // Nos guardamos los caminos mas cortos en una matriz para el output
    vector<vector<int>> minpath(N, vector<int> (N, INT_MAX));
    for (int i = 0; i < N; i++){
        dijkstra(i, minpath, N);
    }
    cout << 1 << "\n";
    for (int i = 0; i <N; i++){
        for (int j = 0; j < N; j++){
            // Si no hay camino, imprimimos INF
            if (minpath[i][j] == INT_MAX) {
                cout << "INF" << " ";
            } else {
                cout << minpath[i][j] - (h[i] - h[j]) << " ";
            }
        }
        cout << "\n";
    }
    return 0;
}



